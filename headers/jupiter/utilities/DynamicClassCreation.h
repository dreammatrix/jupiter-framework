#ifndef DynamicClassCreation_Header
#define DynamicClassCreation_Header

#include <jupiter/Types.h>

namespace Jupiter {
	namespace Utilities {

		struct dynamic_class_table {
			typedef void* (*pfn_classcreate)();

			const char* m_name;
			pfn_classcreate m_pfn_create;
			dynamic_class_table* m_next;
			dynamic_class_table* m_class_it;

			dynamic_class_table(const char* name, pfn_classcreate func, dynamic_class_table** header);

			void* instance_create();
		};

		typedef void (*pfn_classcallback)(const char* name, dynamic_class_table* header, void* userdata);

		extern void* dynamic_class_create(const char* name, dynamic_class_table* header = 0);

		extern dynamic_class_table* dynamic_class_query(const char* name, dynamic_class_table* header = 0);

		extern int dynamic_class_iterate(pfn_classcallback pfncallback, dynamic_class_table* header = 0,
				void* userdata = 0);

		extern dynamic_class_table* dynamic_class_first(dynamic_class_table* header = 0);

		extern dynamic_class_table* dynamic_class_next(dynamic_class_table* header = 0);

		extern int dynamic_class_count(dynamic_class_table* header = 0);

	}  // namespace Utilities
}

#define RegisterDynamicClass(classname) static Jupiter::Utilities::dynamic_class_table classtable_##classname(#classname, classname::reflection_createinstance, NULL)

#define RegisterDynamicClassWithName(name, classname) static Jupiter::Utilities::dynamic_class_table classtable_##classname(name, classname::reflection_createinstance, NULL)

#define ImplementDynamicClass(classname) \
static void* reflection_createinstance( ){ \
	classname *object = new classname; \
	return object; \
} \
virtual const char* get_classname( ){ return #classname; }

#endif // DynamicClassCreation_Header

/*
 * OperationQueue.h
 *
 *  Created on: Sep 8, 2014
 *      Author: unalone
 */

#ifndef OPERATIONQUEUE_H_
#define OPERATIONQUEUE_H_

#include <thread>
#include <functional>
#include <dispatch/dispatch.h>
#include <atomic>

namespace Jupiter {
	namespace Utilities {

		class OperationQueue {
		public:
			OperationQueue (void);
			OperationQueue (dispatch_queue_t queue);
			~OperationQueue ();

			static const OperationQueue& mainQueue ();

			void enqueue (dispatch_block_t operation) const;

			void enqueue (dispatch_block_t operation, float time) const;

			void call (dispatch_block_t operation) const;

			template<typename _OperationType>
			void enqueue (const _OperationType operation) const;

			template<typename _OperationType>
			void enqueue (const _OperationType operation, float time) const;

			template<typename _OperationType>
			void call (const _OperationType operation) const;

		private:
			static OperationQueue _mainQueue;
			static std::atomic_ulong _queueSeed;
			dispatch_queue_t _queue;
			long _queueId = 0;
		};

	} /* namespace Utilities */
} /* namespace Jupiter */

#include <jupiter/utilities/OperationQueue.inl>

#endif /* OPERATIONQUEUE_H_ */

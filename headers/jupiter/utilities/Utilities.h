/*
 * Utilities.h
 *
 *  Created on: Mar 4, 2013
 *      Author: unalone
 */

#ifndef UTILITIES_H_
#define UTILITIES_H_

#include <memory>
#include <boost/any.hpp>

namespace Jupiter {
	namespace Utilities {
		template<typename _T>
		const _T &any_cast(const boost::any &any) {
			static const _T empty = _T();
			if (any.empty()) {
				return empty;
			}

			try {
				return boost::any_cast<const _T&>(any);
			} catch (const boost::bad_any_cast &e) {
				return empty;
			}
		}

		template<typename _T>
		const _T &any_cast_ref(const boost::any &any) {
			static const _T empty = _T();
			if (any.empty()) {
				return empty;
			}

			try {
				return boost::any_cast<const std::reference_wrapper<_T>&>(any).get();
			} catch (const boost::bad_any_cast &e) {
				return empty;
			}
		}
		
		template<typename _T>
		const _T &any_cast_cref(const boost::any &any) {
			static const _T empty = _T();
			if (any.empty()) {
				return empty;
			}
			
			try {
				return boost::any_cast<const std::reference_wrapper<const _T>&>(any).get();
			} catch (const boost::bad_any_cast &e) {
				return empty;
			}
		}

		template<typename _T>
		void get_from_any(const boost::any &any, _T &var) {
			var = any_cast<_T>(any);
		}

		float random(float start, float end);
	}  // namespace utilities
}  // namespace Jupiter

#endif /* UTILITIES_H_ */

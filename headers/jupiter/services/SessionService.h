/*
* @Author: Zhaoming Wang
* @Date:   2015-11-26 14:54:56
* @Last Modified by:   Zhaoming Wang
* @Last Modified time: 2015-12-01
*/

#include <jupiter/services/Types.h>
#include <jupiter/utilities/Utilities.h>
#include <jupiter/utilities/ObserverHub.h>
#include <jupiter/container/container.h>
#include <jupiter/network/network.h>

namespace Jupiter {
	namespace Services {

		/**
		 * Session接口，提供session访问相关的操作封装。
		 */
		struct ISession {
			virtual ~ISession(void) = default;

			/**
			 * session标识，唯一标识一个session。
			 * @return  标识符
			 */
			virtual const std::string &sessionKey(void) const = 0;

			/**
			 * session连接断开事件。
			 */
			virtual const std::shared_ptr<Utilities::ObserverHub<const SessionRef &>> &disconnected() const = 0;

			/**
			 * 恢复一个对话，当一个客户端重新连接时要尝试恢复旧有的session
			 * @param container  容器对象
			 * @param completion 完成回调，通知操作结果，第二个参数是Jupiter::Protocols::SessionApi::ErrorCode类型的错误代码，OK表示成功
			 */
			virtual void restore(const ServiceContainerRef &container, const Action<Jupiter::RpcErrorCode, int32_t> &completion) = 0;

			/**
			 * 更新指定值，如果该值已经存在，则用新的值代替，否则创建新的入口。
			 * @param key  值标识
			 * @param data 值内容
			 * @return 成功返回true，如果连接断开返回false。
			 */
			virtual bool updateData(const std::string &key, const boost::any &data) = 0;

			/**
			 * 获取给定值的内容，该过程是异步的。
			 * @param key        值标识
			 * @param completion 通知回调，第一个参数是Jupiter::Protocols::SessionApi::ErrorCode类型的错误代码，－1表示连接断开，第二个参数是获取的值
			 */
			virtual void getData(const std::string &key, const Action<int32_t, const boost::any &> &completion) const = 0;

			/**
			 * 删除一个值
			 * @param key 值标识
			 */
			virtual void removeData(const std::string &key) = 0;
		};

		/**
		 * 创建一个新的session，但是如果给定客户端已经存在一个session，则返回错误。
		 * @param container  容器对象
		 * @param clientId   客户端标识，可能是设备ID之类的
		 * @param appId      应用标识，平台分配的ID
		 * @param userId     用户ID，平台唯一
		 * @param completion 完成回调，通知操作结果，第二个参数是Jupiter::Protocols::SessionApi::ErrorCode类型的错误代码，OK表示成功，第三个参数是创建的session对象
		 */
		void allocSession(const ServiceContainerRef &container, const std::string &clientId, const std::string &appId, uint64_t userId, const Action<Jupiter::RpcErrorCode, int32_t, const SessionRef &> &completion);

		/**
		 * 恢复一个对话，当一个客户端重新连接时要尝试恢复旧有的session
		 * @param container  容器对象
		 * @param sessionKey 要恢复的session标识
		 * @param completion 完成回调，通知操作结果，第二个参数是Jupiter::Protocols::SessionApi::ErrorCode类型的错误代码，OK表示成功，第三个参数是创建的session对象
		 */
		void restoreSession(const ServiceContainerRef &container, const std::string &sessionKey, const Action<Jupiter::RpcErrorCode, int32_t, const SessionRef&> &completion);

	} // Services
} // Jupiter
/*
* @Author: Zhaoming Wang
* @Date:   2015-11-26
* @Last Modified by:   Zhaoming Wang
* @Last Modified time: 2015-12-01
*/

#include <jupiter/Types.h>

namespace Jupiter {
	namespace Services {
		struct ISession;

		typedef std::shared_ptr<ISession> SessionRef;
	}  // Services
}
/*
 * Filename: /home/unalone/projects/jupiter/jupiter_framework/headers/jupiter/core/PropertyContainer.h
 * Path: /home/unalone/projects/jupiter/jupiter_framework/headers/jupiter/core
 * Created Date: Monday, September 16th 2019, 12:50:17 am
 * Author: unalone
 *
 * Copyright (c) 2019 Your Company
 */
#ifndef PROPERTYCONTAINER_H_
#define PROPERTYCONTAINER_H_

#include <boost/any.hpp>
#include <string>

namespace Jupiter {
	/**
	 * 属性支持接口。
	 * 该接口提供通用的属性支持特性，允许一个对象对外暴露一些动态属性。
	 * 属性类型是一个boost::any类型，因此必须确保属性在设置时和获取时使用完全相同的类型。
	 * 通常，属性在定义的时候应该指明这一点，即，该属性的数据类型是什么，以保证安全存取。
	 * 属性并不一定都是读写的，不同的对象会有不同的实现，可能是读写，也可能是只读或者只写。
	 * 但是对于任何一个实现，该接口的所有方法均需要保证不抛出任何异常，即便是对于只读属性调用了
	 * setter，也只应该默默的失败，并记录在log中。同样，对于一个不存在的属性进行任何访问都是
	 * 合法的，相应的getter应该返回一个空的any，Utilities中的相应方法已经对空any做出了默认
	 * 处理。
	 * 为了避免getter返回空any时导致的逻辑错误，应该避免将一个属性的默认值设为该属性类型的默认
	 * 值，例如，对于enum、int等数值类型，默认值是0，对于字符串，默认是空字符串。
	 */
	struct IPropertyContainer {
		virtual ~IPropertyContainer() = default;

		/**
		 * 设置属性的值。
		 * @param name		属性名。
		 * @param value		待设置的值。
		 */
		virtual void setProperty(const std::string &name, const boost::any &value) = 0;

		/**
		 * 获取属性的值。
		 * @param name		属性名。
		 * @return 如果存在这个属性，则返回属性值，否则返回空any。
		 */
		virtual boost::any getProperty(const std::string &name) const = 0;

		/**
		 * 查询是否存在一个属性。
		 * @param name		属性名。
		 * @return 如果存在这个属性则返回true，否则返回false。
		 */
		virtual bool hasProperty(const std::string &name) const = 0;
	};
}  // namespace Jupiter


#endif  // PROPERTYCONTAINER_H_
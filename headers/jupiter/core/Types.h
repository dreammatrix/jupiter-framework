/*
 * Types.h
 *
 *  Created on: Apr 7, 2015
 *      Author: unalone
 */

#ifndef FRAMEWORKS_HEADERS_JUPITER_CORE_TYPES_H_
#define FRAMEWORKS_HEADERS_JUPITER_CORE_TYPES_H_

#include <jupiter/Types.h>

namespace Jupiter {

	class EventLoop;
	struct IPluginFactory;
	class ResourceManager;

}  // namespace Jupiter

#endif /* FRAMEWORKS_HEADERS_JUPITER_CORE_TYPES_H_ */

/*
 * core.h
 *
 *  Created on: Apr 7, 2015
 *      Author: unalone
 */

#ifndef FRAMEWORKS_HEADERS_JUPITER_CORE_CORE_H_
#define FRAMEWORKS_HEADERS_JUPITER_CORE_CORE_H_

#include <jupiter/core/EventLoop.h>
#include <jupiter/core/IPluginFactory.h>
#include <jupiter/core/PropertyContainer.h>
#include <jupiter/core/PluginLoader.h>
#include <jupiter/core/ResourceManager.h>

#endif /* FRAMEWORKS_HEADERS_JUPITER_CORE_CORE_H_ */

/*
 * PluginLoader.cpp
 *
 *  Created on: Apr 6, 2015
 *      Author: unalone
 */

namespace Jupiter {

	template <typename IPluginType>
	inline Jupiter::PluginLoader<IPluginType>::PluginLoader(const std::string& libPath, const std::string factoryEntryName)
		: _libPath(libPath)
		, _pluginFactoryEntryName(factoryEntryName) {
	}

	template<typename IPluginType>
	inline Jupiter::PluginLoader<IPluginType>::~PluginLoader () {
		if (_handle != nullptr) {
			dlclose(_handle);
			_handle = nullptr;
		}
	}

	template<typename IPluginType>
	inline bool Jupiter::PluginLoader<IPluginType>::initialize () {
		_handle = dlopen(_libPath.c_str(), RTLD_LOCAL);
		if (_handle == nullptr) {
			perror(boost::str(boost::format("Cannot load lib %1%, error is: %2%.\n") % _libPath % dlerror()).c_str());
			return false;
		}

		typedef IPluginFactory *(*ENTRY_FUNC_TYPE) (void);
		ENTRY_FUNC_TYPE entry = (ENTRY_FUNC_TYPE) dlsym(_handle, _pluginFactoryEntryName.c_str());
		if (entry == nullptr) {
			perror(boost::str(boost::format("Cannot find \"%1%\" function in lib %1%, error is: %2%.\n") % _pluginFactoryEntryName % _libPath % dlerror()).c_str());
			return false;
		}

		auto instance = entry();
		if (instance == nullptr) {
			perror(
					boost::str(boost::format("Cannot create plugin factory using \"%1%\" function in lib %1%, error is: %2%.\n") % _pluginFactoryEntryName % _libPath % dlerror()).c_str());
			return false;
		}

		_pluginFactoryInstance.reset(instance);
		return true;
	}

	template<typename IPluginType>
	inline const std::shared_ptr<IPluginType>&& Jupiter::PluginLoader<IPluginType>::createInstance () {
		IPluginType *ptr = nullptr;
		try {
			ptr = static_cast<IPluginType*>(_pluginFactoryInstance->createInstance());
		} catch (...) {
			perror(boost::str(boost::format("Cannot create plugin instance in lib %1%, error is: %2%.\n") % _pluginFactoryEntryName % _libPath % errno).c_str());
			return std::move(std::shared_ptr<IPluginType>());
		}
		return std::move(std::shared_ptr<IPluginType>(ptr));
	}

} /* namespace Jupiter */

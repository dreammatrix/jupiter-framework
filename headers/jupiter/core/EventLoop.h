/*
 * EventLoop.h
 *
 *  Created on: Mar 17, 2015
 *      Author: unalone
 */

#ifndef FRAMEWORKS_JUPITER_SOURCE_CORE_EVENTLOOP_H_
#define FRAMEWORKS_JUPITER_SOURCE_CORE_EVENTLOOP_H_

#include <jupiter/core/Types.h>
#include <event2/event.h>
#include <event2/util.h>
#include <thread>
#include <memory>

namespace Jupiter {

	class EventLoop {
	public:
		virtual ~EventLoop ();

		static EventLoop* mainLoop ();

		event_base* eventBase () const;

		void run();
		void runInThread();
		void stop ();

	private:
		EventLoop ();

		static EventLoop *_mainLoop;
		event_base *_eventBase = nullptr;
		std::thread _eventLoopThread;

		static void eventLoopThreadEntry (void);
	};

} /* namespace Jupiter */

#endif /* FRAMEWORKS_JUPITER_SOURCE_CORE_EVENTLOOP_H_ */

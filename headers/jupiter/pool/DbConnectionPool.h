// /*
//  * DbConnectionPool.h
//  *
//  *  Created on: Mar 13, 2015
//  *      Author: unalone
//  */

// #ifndef FRAMEWORKS_HEADERS_JUPITER_POOL_DBCONNECTIONPOOL_H_
// #define FRAMEWORKS_HEADERS_JUPITER_POOL_DBCONNECTIONPOOL_H_

// #include <jupiter/Types.h>
// #include <jupiter/utilities/OperationQueue.h>
// #include <mysql_connection.h>
// #include <mysql_driver.h>

// namespace Jupiter {

// 	class DbConnectionPool {
// 	public:
// 		DbConnectionPool (void) = default;
// 		~DbConnectionPool ();

// 		bool init (const std::string &url, const std::string &userName, const std::string &password, int maxSize);

// 		const MySqlConnectionRef &&alloc (void);

// 	private:
// 		std::string _url;
// 		std::string _userName;
// 		std::string _password;
// 		int _maxSize = 0;
// 		int _count = 0;
// 		std::list<sql::Connection*> _connections;
// 		Utilities::OperationQueue _queue;

// 		static DbConnectionPool *st_sharedInstance;

// 		void initialize (void);
// 		void free (sql::Connection *connection);
// 		sql::Connection *createConnection ();
// 	};


// } /* namespace Jupiter */

// #endif /* FRAMEWORKS_HEADERS_JUPITER_POOL_DBCONNECTIONPOOL_H_ */

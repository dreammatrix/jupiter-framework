/*
 * ProtocolPool.h
 *
 *  Created on: Feb 25, 2015
 *      Author: unalone
 */

#ifndef FRAMEWORKS_HEADERS_JUPITER_POOL_PROTOCOLPOOL_H_
#define FRAMEWORKS_HEADERS_JUPITER_POOL_PROTOCOLPOOL_H_

#include "jupiter/pool/memory_pool.h"
#include <list>
#include <jupiter/utilities/OperationQueue.h>

namespace Jupiter {

	template<typename _T>
	class ProtocolPool {
	public:
		static std::shared_ptr<_T> construct ();

	private:
		static std::list<_T*> _objects;
		static Utilities::OperationQueue _queue;

		static void free (_T * const object);
	};

	template<typename _T>
	std::list<_T*> ProtocolPool<_T>::_objects;
	template<typename _T>
	Utilities::OperationQueue ProtocolPool<_T>::_queue;

	template<typename _T>
	inline std::shared_ptr<_T> ProtocolPool<_T>::construct () {
		std::shared_ptr<_T> object;
		_queue.call([&] {
			if (_objects.empty()) {
				for (int index = 0; index < 100; ++index) {
					_T *pointer = MemPoolAllocater<_T>::alloc();
					_objects.push_back(pointer);
				}
			}

			auto pointer = _objects.back();
			_objects.pop_back();

			object = std::shared_ptr<_T>(pointer, std::bind(&ProtocolPool<_T>::free, std::placeholders::_1));
		});

		return object;
	}

	template<typename _T>
	inline void ProtocolPool<_T>::free (_T * const object) {
		if (object->SpaceUsed() < 2048) {
			_queue.enqueue([=] {
				_objects.push_back(object);
			});
		} else {
			MemPoolAllocater<_T>::free(object);
		}
	}

} /* namespace Jupiter */

#endif /* FRAMEWORKS_HEADERS_JUPITER_POOL_PROTOCOLPOOL_H_ */

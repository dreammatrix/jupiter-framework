/*
 * Service.h
 *
 *  Created on: Sep 18, 2014
 *      Author: unalone
 */

#ifndef SERVICE_H_
#define SERVICE_H_

#include <boost/property_tree/ptree.hpp>
#include <jupiter/container/Types.h>
#include <jupiter/utilities/OperationQueue.h>
#include <jupiter/network/network.h>
 
namespace Jupiter {

	class Service {
	public:
		virtual ~Service (void) = default;

		virtual bool init (const ServiceContainerRef &container);

		virtual void clientConnected (const std::string &address, const Network::ChannelRef &channel);
	protected:
		Utilities::OperationQueue _queue;
		ServiceContainerRef _container;
		std::vector<Network::ChannelRef> _clients;

		virtual void onClientDisconnected(const Action<> &completion, const Network::ChannelRef &channel);
	};

}  // namespace Jupiter

#endif /* SERVICE_H_ */

/*
 * ServiceContainer.h
 *
 *  Created on: Sep 19, 2014
 *      Author: unalone
 */

#ifndef JUPITER_SERVICECONTAINER_H_
#define JUPITER_SERVICECONTAINER_H_

#include <jupiter/container/Types.h>

namespace Jupiter {

	struct IServiceContainer {
		virtual ~IServiceContainer() = default;

		virtual void acquireService(const std::string &serviceName, const Action<const Network::ChannelRef &> &completion) = 0;
	};

	int serviceMain(int argc, char **argv);

}  // namespace Jupiter

#endif /* JUPITER_SERVICECONTAINER_H_ */

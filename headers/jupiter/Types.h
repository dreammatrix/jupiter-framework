/*
 * Types.h
 *
 *  Created on: May 11, 2014
 *      Author: unalone
 */

#ifndef TYPES_H_
#define TYPES_H_

#include <atomic>
#include <boost/format.hpp>
#include <boost/property_tree/ptree.hpp>
// #include <cppconn/config.h>
// #include <cppconn/connection.h>
// #include <cppconn/driver.h>
// #include <cppconn/exception.h>
// #include <cppconn/prepared_statement.h>
// #include <cppconn/resultset.h>
// #include <cppconn/statement.h>
#include <functional>
#include <map>
#include <memory>
#include <mutex>
// #include <mysql_connection.h>
// #include <mysql_driver.h>
#include <stdint.h>
#include <sys/types.h>
#include <thread>
#include <vector>

extern std::string APP_PREFIX;

namespace Jupiter {
	template <typename... Args>
	using Action = std::function<void(const Args&...)>;

	template <typename ReturnType, typename... Args>
	using Method = std::function<ReturnType(const Args&...)>;

	// class DbConnectionPool;

	// using MySqlConnectionRef = std::shared_ptr<sql::Connection>;
	// using MySqlStatementRef = std::shared_ptr<sql::Statement>;
	// using MySqlStatementPtr = std::unique_ptr<sql::Statement>;
	// using MySqlPreparedStatementRef = std::shared_ptr<sql::PreparedStatement>;
	// using MySqlPreparedStatementPtr = std::unique_ptr<sql::PreparedStatement>;
	// using MySqlResultSetRef = std::shared_ptr<sql::ResultSet>;
	// using MySqlResultSetPtr = std::unique_ptr<sql::ResultSet>;
	// using DbConnectionPoolRef = std::shared_ptr<DbConnectionPool>;

}  // namespace Jupiter

#endif /* TYPES_H_ */

/*
 * Types.h
 *
 *  Created on: Sep 13, 2014
 *      Author: unalone
 */

#ifndef JUPITER_NETWORK_TYPES_H_
#define JUPITER_NETWORK_TYPES_H_

#include <google/protobuf/message.h>
#include <jupiter/Types.h>
#include <jupiter/protocols/Network.pb.h>

namespace Jupiter {
	namespace Network {

//		struct Header {
//			uint16_t length;
//			uint16_t channel;
//
//			Header (void) :
//					channel(0), messageId(0), length(0) {
//			}
//		};

		struct IConnectionManger;

		struct IChannel;
		struct IConnection;
		struct IRpcInvoker;

		using ConnectionMangerRef = std::shared_ptr<IConnectionManger>;
		using ChannelRef = std::shared_ptr<IChannel>;
		using ConnectionRef = std::shared_ptr<IConnection>;
		using RpcInvokerRef = std::shared_ptr<IRpcInvoker>;

		using MessageRef = std::shared_ptr<google::protobuf::Message>;
		using NetworkPacketRef = std::shared_ptr<NetworkPacket>;

		using MessageHandlerType = Action<const ChannelRef &, const NetworkPacketRef &, const MessageRef &>;
		using RpcMethodType = Action<const ChannelRef &, const std::string &, const MessageRef &, const Action<RpcErrorCode, const MessageRef &> &>;
	}  // namespace Network
}  // namespace Jupiter


#endif /* JUPITER_NETWORK_TYPES_H_ */

/*
 * Filename: /home/unalone/projects/jupiter/jupiter_framework/headers/jupiter/network/Constants.h
 * Path: /home/unalone/projects/jupiter/jupiter_framework/headers/jupiter/network
 * Created Date: Sunday, September 22nd 2019, 2:18:52 am
 * Author: unalone
 * 
 * Copyright (c) 2019 Your Company
 */

#ifndef JUPITER_NETWORK_CONSTANTS_H
#define JUPITER_NETWORK_CONSTANTS_H

#include <string>

namespace Jupiter::Network {
	// std::string
	extern const std::string CHANNEL_PROPERTY_REAL_REMOTE_ADDRESS;
	// Jupiter::Network::ChannelRef
	extern const std::string CHANNEL_PROPERTY_PAIRED_CHANNEL;
}

#endif  // JUPITER_NETWORK_CONSTANTS_H

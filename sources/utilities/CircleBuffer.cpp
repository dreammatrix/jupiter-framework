/*
 * CircleBuffer.cpp
 *
 *  Created on: Feb 14, 2015
 *      Author: unalone
 */

#include <jupiter/jupiter.h>

namespace Jupiter {
	namespace Utilities {

		CircleBuffer::CircleBuffer(unsigned long size)
			: _size(size) {
			_buffer = ( char* )MemoryPool::get_instance()->alloc(size);
		}

		CircleBuffer::~CircleBuffer() {
			MemoryPool::get_instance()->free(_buffer);
		}

		bool CircleBuffer::empty(void) const {
			return _head == _tail;
		}

		bool CircleBuffer::full(void) const {
			return _head == ((_tail + 1) % _size);
		}

		unsigned long CircleBuffer::count(void) const {
			if (_tail >= _head) {
				return _tail - _head;
			} else {
				return _size - _head + _tail;
			}
		}

		unsigned long CircleBuffer::space(void) const {
			if (_tail >= _head) {
				return _size - _tail + _head;
			} else {
				return _head - _tail;
			}
		}

		unsigned long CircleBuffer::push(char* buffer, unsigned long count) {
			if (count == 0) {
				return 0;
			}

			if (full()) {
				return count;
			}

			if (_tail >= _head) {
				unsigned long size = min(count, _size - _tail - 1);
				if (size == 0) {
					--count;
					memcpy(_buffer + _tail, buffer, 1);
					_tail = 0;
					return push(buffer + 1, count);
				}

				count -= size;
				memcpy(_buffer + _tail, buffer, size);
				buffer += size;
				_tail += size;

				if (_head > 0 && count > 0 && _tail == _size - 1) {
					--count;
					memcpy(_buffer + _tail, buffer, 1);
					_tail = 0;
					return push(buffer + 1, count);
				}

				return count;
			} else {
				unsigned long size = min(count, _head - _tail - 1);
				memcpy(_buffer + _tail, buffer, size);
				_tail += size;

				count -= size;
				return count;
			}
		}

		void CircleBuffer::erase(unsigned long count) {
			if (count == 0 || empty()) {
				return;
			}

			if (_tail >= _head) {
				_head += min(_tail - _head, count);
			} else {
				auto c = min(_size - _head - 1, count);

				_head += c;

				count -= c;

				if (count > 0) {
					_head = min(count, _tail);
				}
			}
		}

		void CircleBuffer::read(unsigned long count, char* buffer, unsigned long begin) {
			if (count == 0 || empty()) {
				return;
			}

			auto readHead = (_head + begin) % _size;

			if (_tail >= readHead) {
				auto c = min(_tail - readHead - begin, count);
				memcpy(buffer, _buffer + readHead, c);
			} else {
				auto c = min(_size - readHead - 1, count);
				if (c == 0) {
					*buffer = _buffer[ readHead ];
					auto head = _head;
					_head = 0 - begin;
					read(count - 1, buffer + 1, begin + 1);
					_head = head;
				} else {
					memcpy(buffer, _buffer + readHead, c);
					count -= c;

					if (count > 0) {
						c = min(count, _tail - 1);
						memcpy(buffer, _buffer, c);
					}
				}
			}
		}

	}  // namespace Utilities
} /* namespace Jupiter */

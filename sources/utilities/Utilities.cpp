/*
 * Utilities.cpp
 *
 *  Created on: Mar 15, 2013
 *      Author: unalone
 */

#include <boost/random.hpp>
#include <jupiter/jupiter.h>

namespace Jupiter {
	namespace Utilities {
		float random(float start, float end) {
			boost::random::mt19937 rng;
			boost::random::uniform_real_distribution<float> distribution(start, end);
			return distribution(rng);
		}
	}  // namespace utilities
}  // namespace dreammatrix


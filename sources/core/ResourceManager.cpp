/*
 * ResourceManager.cpp
 *
 *  Created on: Mar 24, 2015
 *      Author: unalone
 */

#include <boost/filesystem.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <jupiter/core/ResourceManager.h>
namespace Jupiter {
	ResourceManager *ResourceManager::_sharedInstance = nullptr;

	static const boost::filesystem::path g_configFileRootPath = boost::str(boost::format("%1%/etc/jupiter/") % APP_PREFIX);

	ResourceManager *ResourceManager::sharedInstance() {
		if (_sharedInstance == nullptr) {
			_sharedInstance = new ResourceManager();
		}

		return _sharedInstance;
	}

	bool ResourceManager::loadConfig(const std::string &fileName, boost::property_tree::ptree &root) {
		auto filePath = g_configFileRootPath / fileName;
		try {
			boost::property_tree::read_json(filePath.string(), root);
		} catch (const boost::property_tree::file_parser_error &error) {
			perror(boost::str(boost::format("配置文件[%1%]中含所有错误:\n%2%\n%3%\n%4%\n") % filePath % error.message() % error.filename() % error.line()).c_str());
			root.clear();

			return false;
		}

		return true;
	}

} /* namespace Jupiter */

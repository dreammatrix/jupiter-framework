/*
 * CharacterService.cpp	
 *
 *  Created on: Mar 28, 2015
 *      Author: unalone
 */

#include <jupiter/jupiter.h>

namespace Jupiter {

	bool Service::init(const ServiceContainerRef &container) {
		_container = container;
		return true;
	}

	void Service::clientConnected(const std::string &address, const Network::ChannelRef &channel) {
		_queue.enqueue([=]() { _clients.push_back(channel); });

		channel->closed()->addObserver(
			[this](const Action<> &completion, const Network::ChannelRef &channel) { _queue.enqueue([=]() { onClientDisconnected(completion, channel); }); });
	}

	void Service::onClientDisconnected(const Action<> &completion, const Network::ChannelRef &channel) {
		auto it = std::find(_clients.begin(), _clients.end(), channel);
		if (it != _clients.end()) {
			_clients.erase(it);
		}

		completion();
	}
}  // namespace Jupiter

/* namespace Jupiter */

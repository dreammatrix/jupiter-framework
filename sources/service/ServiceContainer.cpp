/*
 * ServerHostHost.cpp
 *
 *  Created on: Jul 5, 2014
 *      Author: unalone
 */

#include "ServiceContainer.h"
#include <arpa/inet.h>
#include <dlfcn.h>
#include <errno.h>
#include <functional>
#include <jupiter/protocols/JupiterApi.pb.h>
#include <jupiter/protocols/ServiceHost.pb.h>
#include <map>
#include <net/if.h>
#include <net/if_arp.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/types.h>

namespace Jupiter {
	namespace Core {
		void ServiceContainer::start(const std::string &sockName, const std::string &serviceName, const std::string &serviceClassName, const Action<bool> &completion) {
			if (serviceName.empty() || _serviceClassName.empty()) {
				completion(false);
				return;
			}

			_serviceClassName = serviceClassName;
			_connection = Network::connectToServer(sockName, [=](bool isConnected) {
				if (isConnected) {
					const auto &channel = _connection->fork(0);
					if (channel != nullptr) {
						const auto configs = {
							Network::createMessageConfig<Jupiter::AcquireServiceReturnValue>("acquire_service"),
							Network::createEmptyMessageConfig(Jupiter::ServiceHost::MessageId_Exit, std::bind(&ServiceContainer::onExit, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)),
							Network::createMessageConfig<Jupiter::ServiceHost::AllocChannelParameter>("alloc_channel", std::bind(&ServiceContainer::onAllocChannel, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4)),
						};
						channel->setMessageConfig(configs);

						auto parameter = ProtocolPool<Jupiter::ServiceHost::RegisterServiceParameter>::construct();
						parameter->set_name(serviceName);

						channel->send(Jupiter::ServiceHost::MessageId_RegisterService, parameter);
					} else {
						_connection->close();
						_connection = nullptr;
					}
				} else {
					_connection = nullptr;
				}

				completion(isConnected);
			});
		}

		void ServiceContainer::shutdown(void) {
			_connection->close();
			_connection = nullptr;

			_serviceInstance = nullptr;
		}

		void ServiceContainer::acquireService(const std::string &serviceName, const Action<const Network::ChannelRef &> &completion) {
			auto parameter = ProtocolPool<Jupiter::AcquireServiceParameter>::construct();
			parameter->set_servicename(serviceName);

			const auto &serviceChannel = _connection->channelOfId(0);
			if (serviceChannel != nullptr) {
				serviceChannel->invoke("acquire_service", parameter, [=](Jupiter::RpcErrorCode errorCode, const Network::MessageRef &response) {
					if (errorCode == Jupiter::RpcErrorCode_Ok) {
						auto returnValue = dynamic_cast<Jupiter::AcquireServiceReturnValue *>(response.get());
						if (returnValue->errorcode() == Jupiter::AcquireServiceReturnValue::ErrorCode_Ok) {
							const auto &channel = _connection->fork(returnValue->channelid());
							if (channel != nullptr) {
								completion(channel);
							}
							return;
						}
					}

					completion(nullptr);
				});
			} else {
				completion(nullptr);
			}
		}

		void ServiceContainer::onAllocChannel(const Network::ChannelRef &channel, const std::string &methodName, const Network::MessageRef &parameter, const Action<Jupiter::RpcErrorCode, const Network::MessageRef &> &completion) {
			auto realParameter = dynamic_cast<Jupiter::ServiceHost::AllocChannelParameter *>(parameter.get());
			const auto &clientChannel = _connection->fork();
			if (clientChannel != nullptr) {
				if (_serviceInstance != nullptr) {
					_serviceInstance->clientConnected(realParameter->address(), clientChannel);

					auto returnValue = ProtocolPool<Jupiter::ServiceHost::AllocChannelReturnValue>::construct();
					returnValue->set_channelid(clientChannel->channelId());
					completion(Jupiter::RpcErrorCode_Ok, returnValue);
				} else {
					completion(Jupiter::RpcErrorCode_Internal, nullptr);
				}
			}
		}

		ServiceRef ServiceContainer::createServiceInstance() {
			void *object = Utilities::dynamic_class_create(_serviceClassName.c_str());
			Service *instance = reinterpret_cast<Service *>(object);

			if (!instance->init(shared_from_this())) {
				return ServiceRef();
			}
			return ServiceRef(instance);
		}

		void ServiceContainer::onExit(const Network::ChannelRef &channel, const Network::NetworkPacketRef &packet, const Network::MessageRef &message) {
		}
	}  // namespace Core
}  // namespace Jupiter

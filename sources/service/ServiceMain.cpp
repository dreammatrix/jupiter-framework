#include <boost/format.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <dispatch/dispatch.h>
#include <glib.h>
#include <iostream>
#include <unistd.h>
#include "ServiceContainer.h"

using namespace std;

int serviceMain(int argc, char **argv) {
	sigset(SIGCHLD, SIG_IGN);

	//	char *p[1000000];
	//
	//	timeval time;
	//	gettimeofday(&time, NULL);
	//	long begin = (time.tv_sec * 1000) + (time.tv_usec / 1000);
	//
	//	for (int index = 0; index < 1000000; ++index) {
	//		p[index] = (char*)malloc(10000);
	//	}
	//
	//	for (int index = 0; index < 1000000; ++index) {
	//		free(p[index]);
	//	}
	//
	//	gettimeofday(&time, NULL);
	//	long end = (time.tv_sec * 1000) + (time.tv_usec / 1000);
	//
	//	auto first = end - begin;
	//	cout << "tcmalloc: " << first << "ms." <<endl;
	//
	//	gettimeofday(&time, NULL);
	//	begin = (time.tv_sec * 1000) + (time.tv_usec / 1000);
	//
	//	for (int index = 0; index < 1000000; ++index) {
	//		p[index] = (char*)Jupiter::MemoryPool::get_instance()->alloc(10000);
	//	}
	//
	//	for (int index = 0; index < 1000000; ++index) {
	//		Jupiter::MemoryPool::get_instance()->free(p[index]);
	//	}
	//
	//	gettimeofday(&time, NULL);
	//	end = (time.tv_sec * 1000) + (time.tv_usec / 1000);
	//
	//	auto second = end - begin;
	//	cout << "MemoryPool: " << second << "ms." <<endl;

	//	std::vector<int> a = {
	//			10, 9
	//	};
	//	int e;
	//	auto b = e;
	//	auto &c = e;

	//	cout << typeid(b).name() << endl << typeid(c).name() << endl;

	gchar *option_socket_name = nullptr;
	GOptionEntry options[] = {
		{ "socket_name", 's', 0, G_OPTION_ARG_STRING, &option_socket_name, "Path of container's socket file", "" },
		{ nullptr },
	};

	GOptionContext *context;
	GError *err = nullptr;
	GOptionGroup *g_group;
	context = g_option_context_new(nullptr);
	g_group = g_option_group_new("Jupiter svchost native", "Jupiter service host for native service", "Contains a service written in native language (C++)", nullptr, nullptr);
	g_option_context_add_main_entries(context, options, nullptr);
	g_option_context_add_group(context, g_group);
	if (g_option_context_parse(context, &argc, &argv, &err) == FALSE) {
		if (err != nullptr) {
			g_printerr("%s\n", err->message);
			g_error_free(err);
		} else {
			g_printerr("An unknown error occurred\n");
		}

		exit(1);
	}
	g_option_context_free(context);

	boost::property_tree::ptree config;
	if (!Jupiter::ResourceManager::sharedInstance()->loadConfig("./etc/config.json", config)) {
		perror("Failed to load service config!");
		exit(-1);
	}

	auto serviceName = config.get("service.name", "");
	auto serviceClassName = config.get("service.className", "");

	if (serviceName.empty() || serviceClassName.empty()) {
		perror("No service name or service class name configured!");
		exit(-1);
	}

	Jupiter::Core::ServiceContainer container;
	if (container.init(serviceClassName)) {
		container.start(option_socket_name, serviceName, serviceClassName, [](bool isSuccessful) {
			if (!isSuccessful) {
				perror("Failed to start service!");
				exit(-2);
			}
		});

		cout << boost::str(boost::format("Service %1% started.") % serviceClassName) << endl;
		Jupiter::EventLoop::mainLoop()->runInThread();
		dispatch_main();
	} else {
		perror("Failed to init service!");
		return -3;
	}

	return 0;
}
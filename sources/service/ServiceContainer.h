/*
 * ServerHost.h
 *
 *  Created on: Jul 5, 2014
 *      Author: unalone
 */

#ifndef ServiceContainer_H_
#define ServiceContainer_H_

#include <condition_variable>
#include <jupiter/jupiter.h>
#include <mutex>
#include <queue>
#include <sys/socket.h>
#include <thread>
#include <vector>

namespace Jupiter {
	namespace Core {

		class ServiceContainer : public IServiceContainer, public std::enable_shared_from_this<ServiceContainer> {
		public:
			bool init(const std::string &serviceClassName);

			void start(const std::string &sockName, const std::string &serviceName, const std::string &serviceClassName, const Action<bool> &completion);
			void shutdown(void);

			virtual void acquireService(const std::string &serviceName, const Action<const Network::ChannelRef &> &completion) override;

		protected:
			Network::ConnectionRef _connection;
			ServiceRef _serviceInstance;
			std::string _serviceClassName;

			void onAllocChannel(const Network::ChannelRef &channel, const std::string &methodName, const Network::MessageRef &parameter, const Action<Jupiter::RpcErrorCode, const Network::MessageRef &> &completion);

			void onExit(const Network::ChannelRef &channel, const Network::NetworkPacketRef &packet, const Network::MessageRef &message);


			ServiceRef createServiceInstance(void);
		};
	}  // namespace Core
} /* namespace Jupiter */

#endif /* ServiceContainer_H_ */

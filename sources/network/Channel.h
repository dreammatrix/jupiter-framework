/*
 * Channel.h
 *
 *  Created on: Sep 1, 2014
 *      Author: unalone
 */

#ifndef CHANNEL_IMPL_H_
#define CHANNEL_IMPL_H_

#include <atomic>
#include <jupiter/jupiter.h>
#include <map>

namespace Jupiter {
	namespace Network {

		class Channel : public std::enable_shared_from_this<Channel>, public IChannel {
		public:
			Channel(const ConnectionRef &connection, int id);
			~Channel();

			virtual int channelId() const override;
			virtual const ConnectionRef &connection() const override;

			virtual const std::string &address(void) const override;
			void setAddress(const std::string &address);

			virtual short port(void) const override;
			void setPort(short port);

			virtual void setMessageConfig(const std::vector<MessageConfigPtr> &configs) override;

			virtual const std::shared_ptr<Utilities::ObserverHub<const ChannelRef &>> &closed() const override;
			virtual const std::shared_ptr<Utilities::ObserverHub<const ChannelRef &, const NetworkPacketRef &>> &dataArrived() const override;

			virtual void close(const std::function<void()> &completion) override;

			virtual void send(int messageId) override;
			virtual void send(int messageId, const MessageRef &message) override;

			virtual void send(const Network::NetworkPacketRef &packet) override;

			/**
			 * 发起一个rpc调用，所有的调用都是异步的，也就是说，不管成功与否，调用都会立即返回。
			 * @param methodName 要调用的方法名称。
			 */
			virtual void invoke(const std::string &methodName) override;

			/**
			 * 发起一个rpc调用，所有的调用都是异步的，也就是说，不管成功与否，调用都会立即返回。
			 * @param methodName 要调用的方法名称。
			 * @param parameter 方法参数对象。
			 */
			virtual void invoke(const std::string &methodName, const MessageRef &parameter) override;

			/**
			 * 发起一个rpc调用，所有的调用都是异步的，也就是说，不管成功与否，调用都会立即返回。
			 * @param methodName 要调用的方法名称。
			 * @param completion 用于接收调用返回结果的处理器，第一个参数是错误代码，指出调用是否成功；第二个参数是方法返回值对象。
			 */
			virtual void invoke(const std::string &methodName, const Action<RpcErrorCode, const MessageRef &> &completion) override;

			/**
			 * 发起一个rpc调用，所有的调用都是异步的，也就是说，不管成功与否，调用都会立即返回。
			 * @param methodName 要调用的方法名称。
			 * @param parameter 方法参数对象。
			 * @param completion 用于接收调用返回结果的处理器，第一个参数是错误代码，指出调用是否成功；第二个参数是方法返回值对象。
			 */
			virtual void invoke(const std::string &methodName, const MessageRef &parameter, const Action<RpcErrorCode, const MessageRef &> &completion) override;

			void processMessage(const NetworkPacketRef &packet);

			/**
			 * 设置属性的值。
			 * @param name		属性名。
			 * @param value		待设置的值。
			 */
			virtual void setProperty(const std::string &name, const boost::any &value) override;

			/**
			 * 获取属性的值。
			 * @param name		属性名。
			 * @return 如果存在这个属性，则返回属性值，否则返回空any。
			 */
			virtual boost::any getProperty(const std::string &name) const override;

			/**
			 * 查询是否存在一个属性。
			 * @param name		属性名。
			 * @return 如果存在这个属性则返回true，否则返回false。
			 */
			virtual bool hasProperty(const std::string &name) const override;

		private:
			Utilities::OperationQueue _queue;

			std::shared_ptr<Utilities::ObserverHub<const ChannelRef &>> _closed = MemPoolAllocater<Utilities::ObserverHub<const ChannelRef &>>::construct(_queue);
			std::shared_ptr<Utilities::ObserverHub<const ChannelRef &, const NetworkPacketRef &>> _dataArrived = MemPoolAllocater<Utilities::ObserverHub<const ChannelRef &, const NetworkPacketRef &>>::construct(_queue);
			std::map<uint32_t, std::pair<std::string, Action<RpcErrorCode, const MessageRef &>>> _responseHandlers;
			std::map<std::string, boost::any> _properties;
			std::vector<MessageConfigPtr> _messageConfigs;
			ConnectionRef _connection;
			std::atomic_uint_fast32_t _rpcCallSequenceId;
			int _channelId = 0;
			std::string _address;
			short _port;
		};
	} /* namespace Network */
}  // namespace Jupiter

#endif /* CHANNEL_IMPL_H_ */

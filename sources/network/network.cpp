/*
 * network.cpp
 *
 *  Created on: Sep 13, 2014
 *      Author: unalone
 */

#include "Connection.h"
#include <jupiter/jupiter.h>

namespace Jupiter {
	namespace Network {

		const std::string CHANNEL_PROPERTY_REAL_REMOTE_ADDRESS = "@#jupiter#network#channel#property#real#remote#address#@";
		const std::string CHANNEL_PROPERTY_PAIRED_CHANNEL = "@#jupiter#network#channel#property#paired#channel#@";

		bool initialize(void) {
			return true;
		}

		void shutdown(void) {
		}

		ConnectionRef connectionForListen(const std::string &adapterName, ushort port, const Action<const Action<> &, const ConnectionRef &> &connectionArrivalHandler) {
			auto connection = MemPoolAllocater<Connection>::construct();
			connection->connectionArrived()->addObserver(connectionArrivalHandler);
			if (!connection->listenOnAdapter(adapterName, port)) {
				connection = nullptr;
			}
			return connection;
		}

		ConnectionRef connectionForListen(const std::string &path, const Action<const Action<> &, const ConnectionRef &> &connectionArrivalHandler) {
			auto connection = MemPoolAllocater<Connection>::construct();
			connection->connectionArrived()->addObserver(connectionArrivalHandler);
			if (!connection->listenOnLocalSocketFile(path)) {
				connection = nullptr;
			}
			return connection;
		}

		ConnectionRef connectToServer(const std::string &address, ushort port, const Action<bool> &completion) {
			auto connection = MemPoolAllocater<Connection>::construct();
			connection->connectToServer(address, port, completion);
			return connection;
		}

		ConnectionRef connectToServer(const std::string &path, const Action<bool> &completion) {
			auto connection = MemPoolAllocater<Connection>::construct();
			connection->connectToServer(path, completion);
			return connection;
		}

	}  // namespace Network
}  // namespace Jupiter

/*
 * Connection.cpp
 *
 *  Created on: Sep 1, 2014
 *      Author: unalone
 */

#include "Connection.h"
#include "Channel.h"
#include "jupiter/network/Types.h"
#include <arpa/inet.h>
#include <atomic>
#include <cstdio>
#include <errno.h>
#include <jupiter/protocols/Connection.pb.h>
#include <map>
#include <net/if.h>
#include <net/if_arp.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <tuple>
#include <unistd.h>

#include <event2/buffer.h>
#include <event2/bufferevent.h>
#include <event2/util.h>

#include <jupiter/core/EventLoop.h>

namespace Jupiter {
	namespace Network {
		bool Connection::_needStop = false;
		static const ChannelRef __emptyChannelRef;
		std::atomic_uint_fast64_t Connection::_connectionId = 0;

		Connection::Connection() {
			_controlMessageHandlers = {
				{ Jupiter::Connection::MessageId_ChannelClosed, std::bind(&Connection::onRemoteChannelClosedMessage, this, std::placeholders::_1) },
				{ Jupiter::Connection::MessageId_ChannelMarkedPaired, std::bind(&Connection::onChannelMarkedAsPaired, this, std::placeholders::_1) },
			};

			_id = atomic_fetch_add(&_connectionId, 1);
		}

		Connection::~Connection() {
			_status = Status::Destroyed;
			close();
		}
		uint64_t Connection::id(void) const {
			return _id;
		}

		const std::string &Connection::address(void) const {
			return _address;
		}

		short Connection::port(void) const {
			return _port;
		}

		const std::shared_ptr<Utilities::ObserverHub<const ConnectionRef &>> Connection::connectionArrived(void) const {
			return _connectionArrived;
		}

		const std::shared_ptr<Utilities::ObserverHub<const ConnectionRef &>> Connection::connectionClosed(void) const {
			return _connectionClosed;
		}

		void Connection::setPreDispatcher(const Method<std::tuple<ConnectionRef, int>, const ConnectionRef &, int> &processor) {
			_processor = processor;
		}

		const ChannelRef &&Connection::channelOfId(int channelId) const {
			auto result = __emptyChannelRef;
			_queue.call([&]() {
				auto it = _channels.find(channelId);
				if (it != _channels.end()) {
					result = it->second;
				}
			});

			return std::move(result);
		}

		void Connection::freeChannelPtr(Channel *ptr) {
			_channels.erase(ptr->channelId());
			MemPoolAllocater<Channel>::free(ptr);
		}

		const ChannelRef &&Connection::fork() {
			auto result = __emptyChannelRef;

			auto self = shared_from_this();
			_queue.call([&]() {
				auto it = _channels.end();
				int channelId = -1;

				do {
					channelId = _channelId.fetch_add(1);
					if (channelId == 0) {
						continue;
					}

					it = _channels.find(channelId);
				} while ((it != _channels.end() && channelId != INT_MAX) || channelId == 0);

				if (it != _channels.end() && channelId != INT_MAX) {
					auto pointer = MemPoolAllocater<Channel>::alloc(self, channelId);
					std::shared_ptr<Channel> channel(pointer, std::bind(&Connection::freeChannelPtr, this, std::placeholders::_1));
					result = channel;

					_channels.insert({ channelId, channel });
					channel->closed()->addObserver(std::bind(&Connection::onChannelClosed, this, std::placeholders::_1, std::placeholders::_2));
				}
			});

			return std::move(result);
		}

		const ChannelRef &&Connection::fork(int channelId) {
			auto result = __emptyChannelRef;

			auto self = shared_from_this();
			_queue.call([&]() {
				auto it = _channels.find(channelId);
				if (it == _channels.end()) {
					auto pointer = MemPoolAllocater<Channel>::alloc(self, channelId);
					std::shared_ptr<Channel> channel(pointer, std::bind(&Connection::freeChannelPtr, this, std::placeholders::_1));
					_channels.insert({ channelId, channel });

					channel->closed()->addObserver(std::bind(&Connection::onChannelClosed, this, std::placeholders::_1, std::placeholders::_2));

					result = channel;
				}
			});

			return std::move(result);
		}

		void Connection::close(void) {
			_queue.call([&] {
				if (_status != Status::Destroyed) {
					_connectionClosed->fire(nullptr, shared_from_this());
				}

				_status = Status::Closing;

				for (auto &pair : _channels) {
					auto message = ProtocolPool<Jupiter::Connection::ChannelClosedMessage>::construct();
					message->set_channelid(pair.second->channelId());

					sendControlPacket(Jupiter::Connection::MessageId_ChannelClosed, message);

					pair.second->close(nullptr);
				}

				_channels.clear();

				if (_socket != -1) {
					evutil_closesocket(_socket);
					_socket = -1;
				}

				if (_bev != nullptr) {
					bufferevent_free(_bev);
				}

				if (_listener != nullptr) {
					evconnlistener_free(_listener);
				}
			});
		}

		bool Connection::listenOnAdapter(const std::string &adapterName, ushort port) {
			if (adapterName.length() >= 16) {
				perror(boost::str(boost::format("Error: cannot start listening progress, \"adapter "
												"name\" is \"%1%\", which length greater than 16.\n") %
								  adapterName)
						   .c_str());
				return false;
			}

			_socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
			if (evutil_make_socket_nonblocking(_socket) == -1) {
				perror(boost::str(boost::format("Error: cannot start listening progress, \"adapter "
												"name\" is \"%1%\", for which socket cannot be set "
												"to nonblock mode.\n") %
								  adapterName)
						   .c_str());
				return false;
			}

			if (evutil_make_listen_socket_reuseable(_socket) == -1) {
				perror(boost::str(boost::format("Error: cannot start listening progress, \"adapter "
												"name\" is \"%1%\", for which socket cannot be set "
												"to reuseable mode.\n") %
								  adapterName)
						   .c_str());
				return false;
			}

			ifreq req;
			strcpy(req.ifr_name, adapterName.c_str());
			//			memcpy(req.ifr_name, adapterName.data(),
			// adapterName.size());
			if ((ioctl(_socket, SIOCGIFADDR, ( char * )&req))) {
				perror(boost::str(boost::format("Error: cannot start listening progress, "
												"ioctl device \"%1%\" failed to get address") %
								  req.ifr_name)
						   .c_str());
				evutil_closesocket(_socket);
				_socket = -1;
				return false;
			}

			sockaddr_in in;
			in.sin_family = AF_INET;
			in.sin_addr.s_addr = (( struct sockaddr_in * )(&(req.ifr_addr)))->sin_addr.s_addr;
			in.sin_port = htons(port);
#if TARGET_OS_MAC
			in.sin_len = sizeof(sockaddr_in);
#endif
			if (::bind(_socket, ( sockaddr * )&in, sizeof(in)) == -1) {
				perror("Failed bind() \n");
				exit(-1);
			}

			listen(_socket, 256);

			_status = Status::Listening;
			_listener = evconnlistener_new(EventLoop::mainLoop()->eventBase(), &Connection::onClientConnected, this, LEV_OPT_REUSEABLE, 0, _socket);
			return _listener != nullptr;
		}

		bool Connection::listenOnLocalSocketFile(const std::string &path) {
			if (path.length() >= sizeof(sockaddr_un::sun_path)) {
				perror(boost::str(boost::format("Error: cannot start listening progress, \"path\" "
												"is \"%1%\", which length is too long.\n") %
								  path)
						   .c_str());
				return false;
			}

			_socket = socket(PF_LOCAL, SOCK_STREAM, 0);
			if (_socket == -1) {
				perror(boost::str(boost::format("Error: cannot create socket on path: %1%.") % path).c_str());
				return false;
			}

			if (evutil_make_socket_nonblocking(_socket) == -1) {
				perror(boost::str(boost::format("Error: cannot start listening progress, \"path\" "
												"is \"%1%\", for which socket cannot be set to "
												"nonblock mode.\n") %
								  path)
						   .c_str());
				return false;
			}

			sockaddr_un un;
			un.sun_family = AF_UNIX;
			strcpy(un.sun_path, path.c_str());

			auto len = ( int )(strlen(un.sun_path) + 1 + sizeof(un.sun_family));

			unlink(path.c_str());
			if (::bind(_socket, ( sockaddr * )&un, len) == -1) {
				perror("Failed bind() \n");
				exit(-1);
			}

			listen(_socket, 10);

			_status = Status::Listening;
			_listener = evconnlistener_new(EventLoop::mainLoop()->eventBase(), &Connection::onClientConnected, this, LEV_OPT_REUSEABLE, 0, _socket);
			return _listener != nullptr;
		}

		void Connection::connectToServer(const std::string &address, ushort port, const Action<bool> &completion) {
			struct sockaddr serv_addr;
			int len = 0;
			if (evutil_parse_sockaddr_port(boost::str(boost::format("%1%:%2%") % address % port).c_str(), &serv_addr, &len) == -1) {
				perror(boost::str(boost::format("Error: cannont connect to server, \"address\" is "
												"\"%1%:%2%\", for which address cannot be parsed.\n") %
								  address % port)
						   .c_str());
				completion(false);
				return;
			}
			//			serv_addr.sin_family = AF_INET;
			//			serv_addr.sin_port = htons(port);
			//			inet_aton(address.c_str(), &serv_addr.sin_addr);
			//			bzero(&(serv_addr.sin_zero), 8);

			_socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
			if (evutil_make_socket_nonblocking(_socket) == -1) {
				perror(boost::str(boost::format("Error: cannont connect to server, \"address\" is "
												"\"%1%:%2%\", for which socket cannot be set to "
												"nonblock mode.\n") %
								  address % port)
						   .c_str());
				completion(false);
				return;
			}

			struct event_base *base = EventLoop::mainLoop()->eventBase();
			_bev = bufferevent_socket_new(base, _socket, 0);

			bufferevent_setcb(_bev, nullptr, nullptr, &Connection::onSocketEventHappend, this);
			//			bufferevent_enable(_bev, EV_READ);

			_status = Status::Connecting;
			_connectToServerCompletion = completion;
			if (bufferevent_socket_connect(_bev, ( struct sockaddr * )&serv_addr, len) < 0) {
				Utilities::OperationQueue::mainQueue().enqueue([=] { completion(false); });

				bufferevent_free(_bev);
				_socket = -1;
			}
		}

		void Connection::connectToServer(const std::string &path, const Action<bool> &completion) {
			_socket = socket(PF_LOCAL, SOCK_STREAM, 0);
			if (evutil_make_socket_nonblocking(_socket) == -1) {
				perror(boost::str(boost::format("Error: cannot start server, \"path\" is \"%1%\", for "
												"which socket cannot be set to nonblock mode.\n") %
								  path)
						   .c_str());
				exit(-1);
			}

			sockaddr_un un;
			un.sun_family = AF_UNIX;
			strcpy(un.sun_path, path.c_str());

			int len = ( int )(strlen(un.sun_path) + 1 + sizeof(un.sun_family));

			struct event_base *base = EventLoop::mainLoop()->eventBase();
			//			_bev = bufferevent_socket_new(base, _socket,
			// BEV_OPT_CLOSE_ON_FREE);
			_bev = bufferevent_socket_new(base, _socket, 0);

			auto failedHandler = [=] {
				Utilities::OperationQueue::mainQueue().enqueue([=] { completion(false); });

				bufferevent_free(_bev);
				_socket = -1;
			};

			bufferevent_setcb(_bev, nullptr, nullptr, &Connection::onSocketEventHappend, this);
			//			bufferevent_enable(_bev, EV_READ);

			_status = Status::Connecting;
			//			int result = 0;
			//			if ((result = connect(_socket, (struct sockaddr *) &serv_addr,
			// sizeof(struct sockaddr))) < 0) { 				if (errno != EINPROGRESS) {
			//					failedHandler();
			//					return;
			//				}
			//			}
			//
			//			_connectToServerCompletion = completion;
			//			if (result == 0) {
			//				// connection has succeeded immediately
			//				onConnectedToServer();
			//			} else {
			//				_status = Status::Connecting;
			//			}
			//
			//			std::unique_lock<std::recursive_mutex>
			// lk(_connectionsMutex); 			_connections.push_back(shared_from_this());

			_connectToServerCompletion = completion;

			if (bufferevent_socket_connect(_bev, ( struct sockaddr * )&un, len) < 0) {
				Utilities::OperationQueue::mainQueue().enqueue([=] { completion(false); });

				bufferevent_free(_bev);
				_socket = -1;
			}
		}

		void Connection::send(int channelId, const Network::MessageRef &packet) {
			//			auto length = (int16_t) packet->ByteSize();
			//
			//			auto buffer = (char *)
			// MemoryPool::get_instance()->alloc(sizeof(int16_t) + length + 1);
			//
			//			auto netlength = htons(length);
			//			memcpy(buffer, &netlength, sizeof(int16_t));
			//			packet->SerializeToArray(buffer + sizeof(int16_t),
			// length);

			//			auto evb = bufferevent_get_output(_bev);
			//			evbuffer_add(evb, buffer, sizeof(int16_t) + length);

			//			bufferevent_write(_bev, buffer, sizeof(int16_t) +
			// length);
			// bufferevent_flush( _bev, EV_WRITE | EV_READ, BEV_FLUSH );
			//			MemoryPool::get_instance()->free(buffer);

			//			auto output = bufferevent_get_output(_bev);
			//
			auto sendingBuffer = MemPoolAllocater<SendingBuffer>::construct(channelId, packet);
			std::lock_guard<std::mutex> lock(_sendingMutex);
			_sendingBuffers.push_back(sendingBuffer);
		}

		void Connection::sendRaw(int channelId, char *const buffer, int length) {
			auto sendingBuffer = MemPoolAllocater<SendingBuffer>::construct(channelId, buffer, length);
			std::lock_guard<std::mutex> lock(_sendingMutex);
			_sendingBuffers.push_back(sendingBuffer);
		}


		void Connection::markChannelAsPaired(Channel *channel) {
			if (_status == Status::IncomingConnected) {
				return;
			}

			auto message = ProtocolPool<Jupiter::Connection::ChannelMarkedPairedMessage>::construct();
			message->set_channelid(channel->channelId());
			message->set_address(boost::str(boost::format("%1%:%2%") % channel->address() % channel->port()));
			// message->set_port(channel->port());

			sendControlPacket(Jupiter::Connection::MessageId_ChannelMarkedPaired, message);
		}

		/**
		 * 设置属性的值。
		 * @param name		属性名。
		 * @param value		待设置的值。
		 */
		void Connection::setProperty(const std::string &name, const boost::any &value) {
			_properties.insert_or_assign(name, value);
		}

		/**
		 * 获取属性的值。
		 * @param name		属性名。
		 * @return 如果存在这个属性，则返回属性值，否则返回空any。
		 */
		boost::any Connection::getProperty(const std::string &name) const {
			auto it = _properties.find(name);
			if (it == _properties.end()) {
				return boost::any();
			}

			return it->second;
		}

		/**
		 * 查询是否存在一个属性。
		 * @param name		属性名。
		 * @return 如果存在这个属性则返回true，否则返回false。
		 */
		bool Connection::hasProperty(const std::string &name) const {
			auto it = _properties.find(name);
			return it != _properties.end();
		}

		void Connection::removeChannel(int channelId) {
			_queue.enqueue(
				[=] {
					auto it = _channels.find(channelId);
					if (it != _channels.end()) {
						it->second->close([=] { _channels.erase(it); });
					}
				},
				60.0f);
		}

		void Connection::sendControlPacket(int messageId, const MessageRef &message) {
			auto packet = ProtocolPool<Jupiter::Connection::ControlPacket>::construct();
			packet->set_messageid(messageId);

			if (message != nullptr) {
				message->SerializeToString(packet->mutable_payload());
			}

			send(0xffff, packet);
		}


		void Connection::initWithSocket(evconnlistener *listener, int socket) {
			_socket = socket;
			_status = Status::IncomingConnected;

			struct linger linger;
			memset(&linger, 0, sizeof(struct linger));
			auto retVal = setsockopt(_socket, SOL_SOCKET, SO_LINGER, ( const void * )&linger, sizeof(struct linger));
			if (retVal == 0) {}

			_isSending = false;

			//			evutil_make_socket_nonblocking( socket );

			struct event_base *base = evconnlistener_get_base(listener);
			//			_bev = bufferevent_socket_new(base, socket,
			// BEV_OPT_CLOSE_ON_FREE);
			//
			//			bufferevent_setcb(_bev, &Connection::onSocketCanRead, nullptr,
			//&Connection::onSocketEventHappend, this); 			bufferevent_setwatermark(_bev,
			// EV_READ, 0, 4096); 			bufferevent_enable(_bev, EV_READ | EV_WRITE);

			_socketEvent = event_new(base, _socket, EV_READ | EV_WRITE | EV_PERSIST, &Connection::eventCallback, this);
			event_add(_socketEvent, nullptr);

			socklen_t len;
			struct sockaddr_storage addr;
			char ipstr[ INET6_ADDRSTRLEN ];

			len = sizeof addr;
			getpeername(_socket, ( struct sockaddr * )&addr, &len);

			// deal with both IPv4 and IPv6:
			if (addr.ss_family == AF_INET) {
				struct sockaddr_in *s = ( struct sockaddr_in * )&addr;
				_port = ntohs(s->sin_port);
				inet_ntop(AF_INET, &s->sin_addr, ipstr, sizeof ipstr);
				_address = ipstr;
			} else if (addr.ss_family == AF_INET6) {  // AF_INET6
				struct sockaddr_in6 *s = ( struct sockaddr_in6 * )&addr;
				_port = ntohs(s->sin6_port);
				inet_ntop(AF_INET6, &s->sin6_addr, ipstr, sizeof ipstr);
				_address = ipstr;
			} else if (addr.ss_family == AF_UNIX) {
				sockaddr_un *un = ( struct sockaddr_un * )&addr;
				_address = un->sun_path;
			}
		}

		void Connection::onConnectionClosed(void) {
			close();
		}

		void Connection::onChannelClosed(const Action<> &completion, const ChannelRef &channel) {
			_queue.call([&] {
				if (_status == Status::Closing) {
					return;
				}

				int channelId = channel->channelId();
				removeChannel(channelId);

				auto message = ProtocolPool<Jupiter::Connection::ChannelClosedMessage>::construct();
				message->set_channelid(channelId);

				sendControlPacket(Jupiter::Connection::MessageId_ChannelClosed, message);

				completion();
			});
		}

		void Connection::onDataArrived(int length, char *buffer) {
			_queue.enqueue([=]() {
				_reciveBuffer->push(buffer, length);
				MemoryPool::get_instance()->free(buffer);

				if (_reciveBuffer->count() >= sizeof(int16_t) * 2) {
					uint16_t length;
					_reciveBuffer->read(sizeof(int16_t), ( char * )&length);
					length = ntohs(length);

					uint16_t channelId;
					_reciveBuffer->read(sizeof(int16_t), ( char * )&channelId, 2);
					channelId = ntohs(channelId);

					if (_reciveBuffer->count() >= sizeof(int16_t) * 2 + length) {
						char *buffer = ( char * )MemoryPool::get_instance()->alloc(length);

						_reciveBuffer->erase(sizeof(int16_t) * 2);
						_reciveBuffer->read(length, buffer);
						_reciveBuffer->erase(length);

						if (channelId == 0xffff) {
							// Control packet
							auto packet = MemPoolAllocater<Jupiter::Connection::ControlPacket>::construct();
							packet->ParseFromArray(buffer, length);

							MemoryPool::get_instance()->free(buffer);

							processControlPacket(packet);
						} else if (channelId >= 0x8000) {
							// Error
						} else {
							// Data packet
							if (_processor != nullptr) {
								ConnectionRef outConnection;
								int outChannelId;
								std::tie(outConnection, outChannelId) = _processor(shared_from_this(), channelId);
								if (outConnection == nullptr) {
									Connection *c = dynamic_cast<Connection *>(outConnection.get());
									c->sendRaw(outChannelId, buffer, length);
									return;
								}
							}

							auto packet = MemPoolAllocater<NetworkPacket>::construct();
							packet->ParseFromArray(buffer, length);

							MemoryPool::get_instance()->free(buffer);
							auto it = _channels.find(channelId);
							if (it == _channels.end()) {
								return;
							}

							it->second->processMessage(packet);
						}
					}
				}
			});
		}

		// void Connection::onDataArrived(evbuffer *buffer) {
		// 	auto bufferLength = evbuffer_get_length(buffer);
		// 	if (bufferLength >= sizeof(uint16_t) * 2) {
		// 		uint16_t packetLength;
		// 		evbuffer_copyout(buffer, &packetLength, sizeof(uint16_t));
		// 		packetLength = ntohs(packetLength);

		// 		uint16_t channelId;
		// 		evbuffer_ptr ptr;
		// 		ptr.pos = 2;
		// 		evbuffer_copyout_from(buffer, &ptr, &channelId, sizeof(uint16_t));
		// 		packetLength = ntohs(channelId);

		// 		if (bufferLength >= packetLength + sizeof(uint16_t)) {
		// 			evbuffer_drain(buffer, sizeof(uint16_t) * 2);

		// 			std::unique_ptr<char, std::function<void(void *)>> packetBuffer(( char * )MemoryPool::get_instance()->alloc(packetLength), std::bind(&MemoryPool::free,
		// MemoryPool::get_instance(), std::placeholders::_1)); 			evbuffer_remove(buffer, packetBuffer.get(), packetLength);

		// 			auto it = _channels.find(channelId);
		// 			if (it == _channels.end()) {
		// 				printf(
		// 					"[Error] Data recived from %s:%d, on channel:%d, but channel "
		// 					"isn't exist!\n",
		// 					address().c_str(), port(), channelId);
		// 				return;
		// 			}

		// 			auto packet = ProtocolPool<NetworkPacket>::construct();
		// 			packet->ParseFromArray(packetBuffer.get(), packetLength);

		// 			if (it->second->pairedChannel() != nullptr) {
		// 				printf(
		// 					"Data recived from %s:%d, on channel:%d, will be transported to "
		// 					"it's paired channel.\n",
		// 					address().c_str(), port(), channelId);
		// 				it->second->pairedChannel()->send(packet);
		// 			} else {
		// 				printf("Data recived from %s:%d, on channel:%d\n", address().c_str(), port(), channelId);
		// 				it->second->processMessage(packet);
		// 			}
		// 		}
		// 	}
		// }

		void Connection::flushSendingBuffers() {
			std::lock_guard<std::mutex> lock(_sendingMutex);
			if (_sendingBuffers.empty()) {
				return;
			}

			if (_isSending.load()) {
				return;
			}

			_isSending = true;
			bool isSuccessful = false;
			do {
				isSuccessful = (*_sendingBuffers.begin())->send(_socket);
				if (isSuccessful) {
					_sendingBuffers.pop_front();
				} else {
					switch (errno) {
					case EAGAIN: break;

					case ECONNRESET: _sendingBuffers.clear(); break;

					default: break;
					}
				}
			} while (isSuccessful && !_sendingBuffers.empty());

			_isSending = false;
		}

		void Connection::eventCallback(evutil_socket_t socket, short what, void *data) {
			Connection *connection = static_cast<Connection *>(data);

			if (what & EV_READ) {
				char *buffer = ( char * )MemoryPool::get_instance()->alloc(256);
				auto retCode = recv(socket, buffer, 256, 0);
				if (retCode == 0) {
					perror("Failed get header, maybe socket is close?\n");
					connection->close();
					MemoryPool::get_instance()->free(buffer);
				} else if (retCode == -1) {
					perror("Failed get header, some thing goes wrong!\n");

					if (errno != EWOULDBLOCK) {
						connection->close();
					}
					MemoryPool::get_instance()->free(buffer);
				} else {
					connection->onDataArrived(retCode, buffer);
				}
			} else if (what & EV_WRITE) {
				if (connection->_status == Status::Connecting) {
					auto failedHandler = [=] {
						Utilities::OperationQueue::mainQueue().enqueue([=] {
							connection->_connectToServerCompletion(false);
							connection->close();
						});
					};

					int error = 0;
					socklen_t len = sizeof(error);
					if (getsockopt(socket, SOL_SOCKET, SO_ERROR, &error, &len) < 0) {
						failedHandler();
					} else if (error == ECONNREFUSED) {
						perror("Connect to server is refused!");
						failedHandler();
					} else {
						connection->onConnectedToServer();
					}
				} else if (connection->_status == Status::Connected || connection->_status == Status::IncomingConnected) {
					// send buffer
					connection->flushSendingBuffers();
				}
			}
		}

		void Connection::onConnectedToServer(void) {
			Utilities::OperationQueue::mainQueue().enqueue([this] {
				_status = Status::Connected;

				bufferevent_free(_bev);
				_bev = nullptr;

				_socketEvent = event_new(EventLoop::mainLoop()->eventBase(), _socket, EV_READ | EV_WRITE | EV_PERSIST, &Connection::eventCallback, this);
				event_add(_socketEvent, nullptr);

				_connectToServerCompletion(true);
				_connectToServerCompletion = nullptr;
			});
		}

		void Connection::onClientConnected(struct evconnlistener *listener, int sock, struct sockaddr *addr, int len, void *ptr) {
			Connection *connection = static_cast<Connection *>(ptr);

			auto incoming = MemPoolAllocater<Connection>::construct();
			incoming->initWithSocket(listener, sock);

			printf("New connection arrived from %s:%d\n", incoming->address().c_str(), incoming->port());

			connection->_connectionArrived->fire(nullptr, incoming);
		}

		void Connection::onSocketEventHappend(struct bufferevent *bev, short events, void *ctx) {
			Connection *connection = static_cast<Connection *>(ctx);

			auto failedHandler = [=] { Utilities::OperationQueue::mainQueue().enqueue([=] { connection->_connectToServerCompletion(false); }); };

			if (events & BEV_EVENT_CONNECTED) {
				int error = 0;
				socklen_t len = sizeof(error);

				evutil_socket_t fd = bufferevent_getfd(bev);
				if (getsockopt(fd, SOL_SOCKET, SO_ERROR, &error, &len) < 0) {
					failedHandler();
				} else if (error == ECONNREFUSED) {
					perror("Connect to server is refused!");
					failedHandler();
				} else if (events & BEV_EVENT_EOF) {
					size_t len = evbuffer_get_length(bufferevent_get_input(bev));
					perror(boost::str(boost::format("Got a close from %1%.  have %2% left.\n") % "" % ( unsigned long )len).c_str());
					failedHandler();
				} else {
					connection->onConnectedToServer();
				}
			} else if (events & BEV_EVENT_ERROR) {
				printf("Connection operation failed!\n");
				failedHandler();
			} else if (events & BEV_EVENT_EOF) {
				printf("Connection closed by remote peer.\n");
				connection->onConnectionClosed();
			}
		}

		void Connection::processControlPacket(const std::shared_ptr<Jupiter::Connection::ControlPacket> &packet) {
			auto it = _controlMessageHandlers.find(packet->messageid());
			if (it != _controlMessageHandlers.end()) {
				it->second(packet);
			}
		}

		void Connection::onRemoteChannelClosedMessage(const std::shared_ptr<Jupiter::Connection::ControlPacket> &packet) {
			if (packet->payload().empty()) {
				perror("Channel closed message don't have data!");
				close();
				return;
			}

			auto message = ProtocolPool<Jupiter::Connection::ChannelClosedMessage>::construct();
			message->ParseFromString(packet->payload());
			removeChannel(message->channelid());
		}

		void Connection::onChannelMarkedAsPaired(const std::shared_ptr<Jupiter::Connection::ControlPacket> &packet) {
			if (packet->payload().empty()) {
				perror("Channel marked as paired message don't have data!");
				close();
				return;
			}

			auto message = ProtocolPool<Jupiter::Connection::ChannelMarkedPairedMessage>::construct();
			message->ParseFromString(packet->payload());

			const auto &localChannel = channelOfId(message->channelid());
			if (localChannel != nullptr) {
				auto ptr = dynamic_cast<Channel *>(localChannel.get());
				ptr->setAddress(message->address());
			}
		}
	}  // namespace Network
}  // namespace Jupiter

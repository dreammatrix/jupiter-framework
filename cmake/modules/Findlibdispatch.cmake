find_path(
  LIBDISPATCH_INCLUDE_DIR
  dispatch/dispatch.h 
  /usr/include
  /usr/local/include
  )

find_library(
  LIBDISPATCH_LIBRARIES dispatch
  paths $ENV{HOME}/opt/lib/ /usr/lib/ /usr/local/lib/
)

mark_as_advanced(
	LIBDISPATCH_INCLUDE_DIR
	LIBDISPATCH_LIBRARIES
)
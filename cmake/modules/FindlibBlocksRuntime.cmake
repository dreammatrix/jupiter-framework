# - Find libBlockRuntime
# Find the native Tcmalloc library
#
#  BLOCK_RUNTIME_LIBRARIES   - List of libraries when using Tcmalloc.
#  BLOCK_RUNTIME_FOUND       - True if Tcmalloc found.

set(BLOCK_RUNTIME_NAMES BlocksRuntime)

find_library(BLOCK_RUNTIME_LIBRARY NO_DEFAULT_PATH
  NAMES ${BLOCK_RUNTIME_NAMES}
  PATHS ${HT_DEPENDENCY_LIB_DIR} /lib /usr/lib /usr/local/lib /opt/local/lib /usr/lib/x86_64-linux-gnu
)

if (BLOCK_RUNTIME_LIBRARY)
  set(BLOCK_RUNTIME_FOUND TRUE)
  set( BLOCK_RUNTIME_LIBRARIES ${BLOCK_RUNTIME_LIBRARY} )
else ()
  set(BLOCK_RUNTIME_FOUND FALSE)
  set( BLOCK_RUNTIME_LIBRARIES )
endif ()

if (BLOCK_RUNTIME_FOUND)
  message(STATUS "Found libBlockRuntime: ${BLOCK_RUNTIME_LIBRARY}")
else ()
  message(STATUS "Not Found libBlockRuntime: ${BLOCK_RUNTIME_LIBRARY}")
  if (BLOCK_RUNTIME_FIND_REQUIRED)
    message(STATUS "Looked for libBlockRuntime libraries named ${BLOCK_RUNTIME_NAMES}.")
    message(FATAL_ERROR "Could NOT find libBlockRuntime library")
  endif ()
endif ()

mark_as_advanced(
  BLOCK_RUNTIME_LIBRARY
  )
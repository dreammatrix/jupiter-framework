#! /bin/bash

command=""
argCount=$#
if [ -z "$1"  ] ; then
	if [ ! -d "build" ]; then
		command="cmake -S . -B ./build -DCMAKE_INSTALL_PREFIX=~/opt &&"
	fi
	command="$command cd build && make -j"
else
	while [ ! -z "$1" ]; do
		case $1 in
			--clear|-d)
				echo $1
				if [ ! -z "$command"  ] ; then
					command="&& ${command}"
				fi
				command="rm -Rf build && cmake -S . -B ./build -DCMAKE_INSTALL_PREFIX=~/opt ${command}"

				if [ $argCount -ne 2 ] ; then
					command="$command && cd build && make -j && cd .."
				fi
				;;
			--install|-i)
				echo $1
				if [ ! -z "$command"  ] ; then
					command="${command} &&"
				else
					if [ ! -d "build" ]; then
						command="cmake -S . -B ./build -DCMAKE_INSTALL_PREFIX=~/opt &&"
					fi
				fi
				command="${command} cd build && make -j && make install && cd .."
				;;
		esac
	shift
	done

fi

echo $command
`which bash` -c  "$command"

